import { Students } from './../interfaces/students';
import { StudentsService } from './../students.service';
import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'addForm',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {

  @Input() name:string;
  @Input() math:number;
  @Input() psycho:number;
  @Input() id:string;
  varPay:Array<boolean>  = [true,false];
  @Input() payed: string;
  errormassage:string ="";
  @Output() update = new EventEmitter<Students>();
  @Output() closeEdit = new EventEmitter<null>();

  updateParent(){
    if (this.math<0 || this.math> 100) {
      this.errormassage = "math grade must be between 0 to 100"

    }else{
      console.log(this.payed)
    let student:Students = {id:this.id, name: this.name, math:this.math, psycho:this.psycho, payed:this.payed};
    this.update.emit(student)
      this.name = null;
      this.math = null;
      this.psycho = null;
      this.payed = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  onSubmit(){ 

  }  


  constructor(public studentsService:StudentsService) { }

  ngOnInit(): void {
  }

}
