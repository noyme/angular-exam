import { PredictService } from './../predict.service';
import { StudentsService } from './../students.service';
import { Students } from './../interfaces/students';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'math', 'psycho', 'payed' , 'button' , 'drop'];

  students:Students[];
  students$;
  resArr = ['Will not finish', 'Finish']

  constructor(private studentsService:StudentsService, public predictService:PredictService) { }

  add(student:Students){
    this.studentsService.addStudent(student.name,student.math,student.psycho,student.payed)
  }

  predict(index){
    this.students[index].result = 'Will not finish';
    this.predictService.predict(this.students[index].math, this.students[index].psycho, this.students[index].payed).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Finish';
        } else {
          var result = 'Will not finish'
        }
        console.log(result);
        this.students[index].result = result}
    );  
  }
  updateResult(index){
    this.students[index].saved = true; 
    this.studentsService.updateRsult(this.students[index].id,this.students[index].result);
  }

  ngOnInit(): void {

    this.students$ = this.studentsService.getStudents();
    this.students$.subscribe(
      docs => {         
        this.students = [];
        var i = 0;
        for (let document of docs) {
          console.log(i++); 
          const student:Students = document.payload.doc.data();
          student.id = document.payload.doc.id;
             this.students.push(student); 
        }                        
      })
  

    }
  
}
