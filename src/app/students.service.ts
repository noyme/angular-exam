import { Injectable } from '@angular/core';
import {AngularFirestoreCollection, AngularFirestore} from '@angular/fire/firestore'
import { Observable } from 'rxjs';
import { Students } from './interfaces/students';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection;

  
getStudents(): Observable<any[]> {
  this.studentsCollection = this.db.collection(`students`)
  return this.studentsCollection.snapshotChanges();    
} 
addStudent(name, math, psycho, payed){
  const student:Students = {name:name,math:math, psycho:psycho, payed:payed}
  this.studentsCollection.add(student);

}
updateRsult(id:string,result:string){
  this.db.doc(`students/${id}`).update(
    {
      result:result
    })
  }


  constructor(private db:AngularFirestore) { }
}
