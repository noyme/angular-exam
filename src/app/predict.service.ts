import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  url = 'https://6vq6kaqwbk.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(math:number, psycho:number, payed:string):Observable<any>{
    let json = {
      "data": 
        {
          "math": math,
          "psycho": psycho,
          "payed":payed

        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
