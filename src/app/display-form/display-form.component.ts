import { StudentsService } from './../students.service';
import { Students } from './../interfaces/students';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-form',
  templateUrl: './display-form.component.html',
  styleUrls: ['./display-form.component.css']
})
export class DisplayFormComponent implements OnInit {

  constructor(private studentsService:StudentsService) { }

  add(student:Students){
    this.studentsService.addStudent(student.name,student.math,student.psycho,student.payed)
  }

  ngOnInit(): void {
  }

}
