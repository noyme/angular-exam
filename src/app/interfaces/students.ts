export interface Students {
    id?:string;
    name:string,
    math:number,
    psycho: number,
    payed:string,
    result?:string,
    saved?:Boolean;
    
}
