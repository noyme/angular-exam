import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;



  constructor(public auth:AuthService, private router:Router) { }

  onSubmit(){
    this.auth.signUp(this.email, this.password)
    .then((result) => {
      window.alert("You have been successfully registered!");
      console.log(result.user);
      this.router.navigate(['/welcome']);
      }).catch((error) => {
        window.alert(error.message)
      });
  }

  ngOnInit(): void {
  }

}
